# ZHSI (Zibo737 Glass Cockpit Software Suite)

> ZHSI is a Glass Cockpit Software suite for ZiboMod 737 in XPlane inspired by XHSI

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```



Pre-build v2.2.0 can be downloaded here: (https://gitlab.com/sum1els737/zhsi-releases)

The plugin is available to download under the releases above and the source code for that is here : https://gitlab.com/sum1els737/zhsi-plugin (slightly modified ExtPlane plugin - thanks to Ville Ranki and others.. (https://github.com/vranki/ExtPlane)
![](header.png)
   
## Meta
Andre Els – [https://www.facebook.com/sum1els737](https://www.facebook.com/sum1els737)

