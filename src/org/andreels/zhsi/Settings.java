/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URL;
import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.GridLayout;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;
import javax.swing.Box;
import java.awt.Component;
import java.awt.Desktop;

import javax.swing.ImageIcon;
import java.awt.Dimension;
import javax.swing.JSlider;
import java.awt.TextArea;
import javax.swing.JTextArea;

public class Settings extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger("org.andreels.zhsi");
	private Image logo_image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("ZHSI_logo.png"));
	private JTextField txtXPip;
	private JTextField txtXPexec;
	private JTextField txtEgpwsDir;
	private Desktop desktop;
	private ZHSIPreferences preferences;
	private boolean EGPWSDirOK = false;
	private File selectedEGPWSDir;
	private JTextField txtUserName;
	private JTextField txtUserLastName;
	private JTextField txtUserKey;
	private JTextArea txtAreaUserKey;
	private JTextField txtUserEmail;

	/**
	 * Create the dialog.
	 */
	public Settings() {
		setBounds(100, 100, 600, 600);
		this.setIconImage(this.logo_image);
		this.setTitle("ZHSI Settings");
		this.setResizable(false);
		this.setLocationRelativeTo(getParent());
		this.preferences = ZHSIPreferences.getInstance();
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Apply");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						if(validIP(txtXPip.getText())) {
							preferences.set_preference(ZHSIPreferences.PREF_EXTPLANE_SERVER, txtXPip.getText());
							preferences.set_preference(ZHSIPreferences.USER_FIRST_NAME, txtUserName.getText());
							preferences.set_preference(ZHSIPreferences.USER_LAST_NAME, txtUserLastName.getText());
							preferences.set_preference(ZHSIPreferences.USER_EMAIL, txtUserEmail.getText());
							preferences.set_preference(ZHSIPreferences.USER_KEY, txtAreaUserKey.getText());
							txtXPip.setForeground(Color.BLACK);
							JOptionPane.showMessageDialog(null, "Please restart ZHSI for changes to take affect", "Please restart ZHSI", JOptionPane.INFORMATION_MESSAGE);
							dispose();
							//System.exit(0);
						}else {
							//preferences.set_preference(ZHSIPreferences.PREF_EXTPLANE_SERVER, "");
							txtXPip.setForeground(Color.RED);
							txtXPip.setText("IP address not valid!");
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton closeButton = new JButton("Close");
				closeButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseReleased(MouseEvent e) {
						dispose();
					}
				});
				closeButton.setActionCommand("Close");
				buttonPane.add(closeButton);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new EmptyBorder(20, 10, 20, 10));
			getContentPane().add(panel, BorderLayout.CENTER);
			panel.setLayout(new GridLayout(7, 1, 2, 2));
			{
				JPanel panelXPIp = new JPanel();
				panelXPIp.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelXPIp);
				{
					JLabel lblXPip = new JLabel("X-Plane IP Address:");
					lblXPip.setHorizontalAlignment(SwingConstants.LEFT);
					panelXPIp.add(lblXPip);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(20);
					panelXPIp.add(horizontalStrut);
				}
				{
					txtXPip = new JTextField();
					txtXPip.setText(this.preferences.get_preference(ZHSIPreferences.PREF_EXTPLANE_SERVER));
					txtXPip.setHorizontalAlignment(SwingConstants.CENTER);
					panelXPIp.add(txtXPip);
					txtXPip.setColumns(15);
				}
				{
					JLabel lblXPipHelp = new JLabel("");
					lblXPipHelp.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							openWebPage("https://bitbucket.org/sum1els737/zhsi/wiki/Home#markdown-header-x-plane-ip-address");
						}
					});
					lblXPipHelp.setToolTipText("Enter the IP Address of your X-Plane PC (Click for more info...)");
					lblXPipHelp.setIcon(new ImageIcon(Settings.class.getResource("/org/andreels/zhsi/resources/Button-help-icon.png")));
					panelXPIp.add(lblXPipHelp);
				}
			}
			{
				JPanel panelXPexec = new JPanel();
				panelXPexec.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelXPexec);
				{
					JLabel lblXPexec = new JLabel("X-Plane Excecutable:");
					panelXPexec.add(lblXPexec);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(15);
					panelXPexec.add(horizontalStrut);
				}
				{
					txtXPexec = new JTextField();
					txtXPexec.setEditable(false);
					txtXPexec.setText(this.preferences.get_preference(ZHSIPreferences.PREF_APTNAV_DIR));
					panelXPexec.add(txtXPexec);
					txtXPexec.setColumns(15);
				}
				{
					JButton btnXPbrowse = new JButton("Browse");
					btnXPbrowse.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							
							JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
							int returnValue = fc.showSaveDialog(null);
							if(returnValue == JFileChooser.APPROVE_OPTION) {
								File selectedXPDir = fc.getCurrentDirectory();
								File xpwin = new File(selectedXPDir + "/X-Plane.exe");
								File xpmac = new File(selectedXPDir + "/X-Plane.app");
								File xplin = new File(selectedXPDir + "/X-Plane-x86_64");
								if((xpwin.exists() || xpmac.exists() || xplin.exists())) {
									//lblXPlaneDirectory.setForeground(Color.GREEN);
									txtXPexec.setText("" + selectedXPDir);
									preferences.set_preference(ZHSIPreferences.PREF_APTNAV_DIR, txtXPexec.getText());
									txtXPexec.setForeground(Color.BLACK);

								}else {
									//lblXPlaneDirectory.setForeground(Color.RED);
									txtXPexec.setForeground(Color.RED);
									txtXPexec.setText("Invalid X-Plane Directory");
									preferences.set_preference(ZHSIPreferences.PREF_APTNAV_DIR, "");

								}
								
							}
						}
					});
					panelXPexec.add(btnXPbrowse);
				}
				{
					JLabel lblXPexecHelp = new JLabel("");
					lblXPexecHelp.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							openWebPage("https://bitbucket.org/sum1els737/zhsi/wiki/Home#markdown-header-x-plane-folder");
						}
					});
					lblXPexecHelp.setToolTipText("Browse for and click on the X-Plane executable, i.e. X-Plane.exe (Click for more help ...)");
					lblXPexecHelp.setIcon(new ImageIcon(Settings.class.getResource("/org/andreels/zhsi/resources/Button-help-icon.png")));
					panelXPexec.add(lblXPexecHelp);
				}
			}
			{
				JPanel panelEGPWS = new JPanel();
				panelEGPWS.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelEGPWS);
				{
					JLabel lblEgpwsDir = new JLabel("EGPWS Directory:");
					panelEGPWS.add(lblEgpwsDir);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(34);
					panelEGPWS.add(horizontalStrut);
				}
				{
					txtEgpwsDir = new JTextField();
					txtEgpwsDir.setEditable(false);
					txtEgpwsDir.setText(this.preferences.get_preference(ZHSIPreferences.PREF_EGPWS_DB_DIR));
					panelEGPWS.add(txtEgpwsDir);
					txtEgpwsDir.setColumns(15);
				}
				{
					JButton btnEgpwsBrowse = new JButton("Browse");
					btnEgpwsBrowse.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							JFileChooser fc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
							fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
							int returnValue = fc.showSaveDialog(null);
							if(returnValue == JFileChooser.APPROVE_OPTION) {
								if(isMac()) {
									selectedEGPWSDir = fc.getCurrentDirectory();
								}else {
									selectedEGPWSDir = fc.getSelectedFile();
								}
								File[] listFiles = selectedEGPWSDir.listFiles();
								for(int i = 0; i < listFiles.length; i++) {
									if(listFiles[i].isFile()) {
										String tempFile = listFiles[i].getName();
										if(tempFile.endsWith("10g")) {
											EGPWSDirOK = true;
										}else {
											EGPWSDirOK = false;
										}
									}
								}
								if(selectedEGPWSDir.exists() && EGPWSDirOK) {
									txtEgpwsDir.setText("" + selectedEGPWSDir);
									preferences.set_preference(ZHSIPreferences.PREF_EGPWS_DB_DIR, txtEgpwsDir.getText());
									txtEgpwsDir.setForeground(Color.BLACK);

								}else {
	
									txtEgpwsDir.setForeground(Color.RED);
									txtEgpwsDir.setText("Enter valid EGPWS Directory");
									preferences.set_preference(ZHSIPreferences.PREF_EGPWS_DB_DIR, "");
								}
							}
						}
					});
					panelEGPWS.add(btnEgpwsBrowse);
				}
				{
					JLabel lblEgpwsHelp = new JLabel("");
					lblEgpwsHelp.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseReleased(MouseEvent e) {
							openWebPage("https://bitbucket.org/sum1els737/zhsi/wiki/Home#markdown-header-terrain");
						}
					});
					lblEgpwsHelp.setIcon(new ImageIcon(Settings.class.getResource("/org/andreels/zhsi/resources/Button-help-icon.png")));
					lblEgpwsHelp.setToolTipText("Browse and select the directory where you downloaded the NOAA Tiles. (Click for more help ...)");
					panelEGPWS.add(lblEgpwsHelp);
				}
			}
			{
				JPanel panelUserName = new JPanel();
				panelUserName.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelUserName);
				{
					JLabel lblUserName = new JLabel("First Name:");
					panelUserName.add(lblUserName);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(77);
					horizontalStrut.setMinimumSize(new Dimension(75, 0));
					panelUserName.add(horizontalStrut);
				}
				{
					txtUserName = new JTextField();
					txtUserName.setText(this.preferences.get_preference(ZHSIPreferences.USER_FIRST_NAME));
					panelUserName.add(txtUserName);
					txtUserName.setColumns(10);
				}
			}
			{
				JPanel panelUserLastName = new JPanel();
				panelUserLastName.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelUserLastName);
				{
					JLabel lblUserLastName = new JLabel("Last Name:");
					panelUserLastName.add(lblUserLastName);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(80);
					panelUserLastName.add(horizontalStrut);
				}
				{
					txtUserLastName = new JTextField();
					txtUserLastName.setText(this.preferences.get_preference(ZHSIPreferences.USER_LAST_NAME));
					panelUserLastName.add(txtUserLastName);
					txtUserLastName.setColumns(10);
				}
			}
			{
				JPanel panelUserEmail = new JPanel();
				panelUserEmail.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelUserEmail);
				{
					JLabel lblUserEmail = new JLabel("Email Address:");
					panelUserEmail.add(lblUserEmail);
				}
				{
					Component horizontalStrut = Box.createHorizontalStrut(53);
					panelUserEmail.add(horizontalStrut);
				}
				{
					txtUserEmail = new JTextField();
					txtUserEmail.setText(this.preferences.get_preference(ZHSIPreferences.USER_EMAIL));
					panelUserEmail.add(txtUserEmail);
					txtUserEmail.setColumns(20);
				}
			}
			{
				JPanel panelUserKey = new JPanel();
				panelUserKey.setLayout(new FlowLayout(FlowLayout.LEFT));
				panel.add(panelUserKey);
				{
					JLabel lblUserKey = new JLabel("User Key:");
					panelUserKey.add(lblUserKey);
				}
				{
					Border border = BorderFactory.createLineBorder(Color.BLACK);
					txtAreaUserKey = new JTextArea();
					txtAreaUserKey.setLineWrap(true);
					txtAreaUserKey.setRows(2);
					txtAreaUserKey.setColumns(60);
					txtAreaUserKey.setText(this.preferences.get_preference(ZHSIPreferences.USER_KEY));
					txtAreaUserKey.setBorder(border);
					panelUserKey.add(txtAreaUserKey);
				}
				
				{
					//txtUserKey = new JTextField();
					//txtUserKey.setText(this.preferences.get_preference(ZHSIPreferences.USER_KEY));
					//panel_1.add(txtUserKey);
					//txtUserKey.setColumns(40);
					
				}
			}
		}
	}
	public void openWebPage(String url) {
		
		desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(new URL(url).toURI());
			}catch(Exception e) {
				logger.info("Invalid URL: " + e.toString());
			}
		}
	}
	private boolean validIP (String ip) {
		try {
			if ( ip == null || ip.isEmpty() ) {
				return false;
			}

			String[] parts = ip.split( "\\." );
			if ( parts.length != 4 ) {
				return false;
			}

			for ( String s : parts ) {
				int i = Integer.parseInt( s );
				if ( (i < 0) || (i > 255) ) {
					return false;
				}
			}
			if ( ip.endsWith(".") ) {
				return false;
			}

			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	private boolean isMac() {
		// return (System.getProperty("mrj.version") != null);
		String OS = System.getProperty("os.name").toLowerCase();
		return (OS.indexOf("mac") >= 0);
	}

}
