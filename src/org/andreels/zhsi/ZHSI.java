/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * Main class (entry point)
 * 
 * Inspired by XHSI (http://xhsi.sourceforge.net)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi;

import java.io.File;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ZHSI extends Application {
	
	private static final String LIB_DIR = System.getProperty("user.dir") + "/ZHSI_libs/jfoenix-8.0.7.jar";
	
	private static Logger logger = Logger.getLogger("org.andreels.zhsi");
	private String version = "2.2.1";
	private ZHSIPreferences preferences;
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		
		this.preferences = ZHSIPreferences.getInstance();
		logger.info("ZHSI version " + version + " started");
		
		if(new File(LIB_DIR).exists()) {
			logger.info("ZHSI Libraries found");
		} else {
			logger.info("ZHSI Libraries not found - You need the \"ZHSI_libs\" folder in the same location as this application");
			JOptionPane.showMessageDialog(null, "ZHSI Libraries (folder: /ZHSI_libs) not found, unable to start", "ZHSI Startup Error", JOptionPane.ERROR_MESSAGE);
		}
		
		primaryStage.setTitle("ZHSI " + version);
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(ZHSI.class.getResource("zhsifx.fxml"));
		AnchorPane mainPane = loader.load();
		//BorderPane mainPane = loader.load();
		Scene mainScene = new Scene(mainPane, 800,600);
		//mainScene.getStylesheets().add(getClass().getResource("ZHSI.css").toExternalForm());
		mainScene.getStylesheets().add(ZHSI.class.getResource("zhsifx.css").toExternalForm());
		ZHSIController controller = loader.getController();
		controller.setStage(primaryStage);
		controller.setVersion(version);
		primaryStage.setScene(mainScene);
		primaryStage.setResizable(true);
		//primaryStage.setMinWidth(630.0);
		//primaryStage.setMinHeight(450.0);
		//primaryStage.setMaxHeight(1200.0);
		primaryStage.getIcons().add(new Image(ZHSI.class.getResourceAsStream("/org/andreels/zhsi/ZHSI_logo.png")));
		primaryStage.setOnCloseRequest(event -> {
			controller.disconnect();
			Platform.exit();
			System.exit(0);
		});

		if(this.preferences.get_preference(ZHSIPreferences.PREF_START_MINIMIZED).equals("true")) {
			primaryStage.setIconified(true);
		}

		primaryStage.show();
		primaryStage.xProperty().addListener((obs, oldVal, newVal) -> {
			this.preferences.set_preference(ZHSIPreferences.PREF_UI_POS_X, "" + Math.round(primaryStage.getX()));
		});
		primaryStage.yProperty().addListener((obs, oldVal, newVal) -> {
			this.preferences.set_preference(ZHSIPreferences.PREF_UI_POS_Y, "" + Math.round(primaryStage.getY()));
		});
		
	}
//
//	ModelFactory model_instance;
//	private static final String ZHSI_VERSION = "1.18.0";
//	private ZHSIPreferences preferences;
//	private static Logger logger = Logger.getLogger("org.andreels.zhsi");
//
//	public ZHSI() throws Exception {
//		
//
//		this.preferences = ZHSIPreferences.getInstance();
//
//		if (this.preferences.get_preference(ZHSIPreferences.PREF_OPENGL).equals("true")) {
//			System.setProperty("sun.java2d.opengl", "true");
//			System.setProperty("sun.java2d.d3d", "false");
//		} else if (this.preferences.get_preference(ZHSIPreferences.PREF_OPENGL).equals("True")) {
//			System.setProperty("sun.java2d.opengl", "True");
//			System.setProperty("sun.java2d.d3d", "false");
//		}
//
//		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//
//		Handler handler = new ConsoleHandler();
//		handler.setLevel(Level.ALL);
//		handler.setFormatter(new ZHSILogFormatter());
//		handler.setFilter(null);
//		logger.addHandler(handler);
//
//		// TODO : check if ZHSI.log is writable
//		handler = new FileHandler(System.getProperty("user.dir") + "/ZHSI.log");
//		handler.setLevel(Level.ALL);
//		handler.setFormatter(new ZHSILogFormatter());
//		handler.setFilter(null);
//		logger.addHandler(handler);
//
//		logger.setLevel(Level.ALL);
//		logger.setUseParentHandlers(false);
//
//		logger.info("ZHSI version " + ZHSI_VERSION + " started");
//
//		Runtime java_run = Runtime.getRuntime();
//		logger.config("Free  Memory: " + (java_run.freeMemory() / 1024 / 1024) + "M");
//		logger.config("Total Memory: " + (java_run.totalMemory() / 1024 / 1024) + "M");
//		logger.config("Max  Memory: " + (java_run.maxMemory() / 1024 / 1024) + "M");
//
//		ZHSIStatus.status = ZHSIStatus.STATUS_STARTUP;
//
//		logger.config("Selected loglevel: " + this.preferences.get_preference(ZHSIPreferences.PREF_LOGLEVEL));
//		logger.setLevel(Level.parse(this.preferences.get_preference(ZHSIPreferences.PREF_LOGLEVEL)));
//
//		model_instance = new XPlaneModelFactory();
//		// creates a new ExtPlaneInterface instance if not already created
//		ExtPlaneInterface iface = ExtPlaneInterface.getInstance();
//		// run it in a new thread
//		Thread ext_iface = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					iface.start();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//		ext_iface.start();
//
//		// weather receiver
//		XPlaneDataPacketDecoder packet_decoder = new XPlaneDataPacketDecoder(model_instance);
//		XPlaneWeatherReceiver weather_receiver = new XPlaneWeatherReceiver(48003, false, null);
//		weather_receiver.add_reception_observer(packet_decoder);
//		weather_receiver.start();
//
//		
//		long oldAptVersion = Long.parseLong(this.preferences.get_preference(ZHSIPreferences.APTNAV_VERSION));
//		String apt_file_location = preferences.get_preference(ZHSIPreferences.PREF_APTNAV_DIR) + "/Resources/default scenery/default apt dat/Earth nav data/apt.dat";
//		long currentAptVersion = new File(apt_file_location).lastModified();
//		AptFileParser apt = new AptFileParser();
//		
//		if (new File("./zhsi_apt.dat").exists() && new File("./zhsi_rwy.dat").exists()) {
//			//check if current is newer
//			if((currentAptVersion > oldAptVersion) || (currentAptVersion != oldAptVersion)) {
//				logger.info("APT dat file: zhsi_apt.dat, exists, but outdated, trying to generate a new file...");
//				logger.info("RWY dat file: zhsi_rwy.dat, exists, but outdated, trying to generate a new file...");
//				apt.parseFile();
//				this.preferences.set_preference(ZHSIPreferences.APTNAV_VERSION, ""+currentAptVersion);
//			}else {
//				logger.info("APT dat file: current zhsi_apt.dat up to date, skipping file generation");
//				logger.info("RWY dat file: current zhsi_rwy.dat up to date, skipping file generation");
//			}
//		} else {
//			apt.parseFile();
//		}
//
//		// navdata builder
//		Thread nav_builder = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				AptNavXP900DatNavigationObjectBuilder nob;
//				try {
//					nob = new AptNavXP900DatNavigationObjectBuilder(
//							preferences.get_preference(ZHSIPreferences.PREF_APTNAV_DIR));
//					if (!ZHSIStatus.nav_db_status.equals(ZHSIStatus.STATUS_NAV_DB_NOT_FOUND)) {
//						nob.read_all_tables();
//					}
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		});
//		nav_builder.start();
//
//		Thread elevation_builder = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					GlobeElevationBuilder geb = new GlobeElevationBuilder(
//							preferences.get_preference(ZHSIPreferences.PREF_EGPWS_DB_DIR));
//					if (!ZHSIStatus.nav_db_status.equals(ZHSIStatus.STATUS_EGPWS_DB_NOT_FOUND)) {
//						geb.map_database();
//					}
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//
//		});
//		elevation_builder.start();
//
//		// finally start the UI
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					UI3 ui = new UI3(model_instance, "ZHSI version: " + ZHSI_VERSION);
//					// ui.setVisible(true);
//					// ui.setState(Frame.ICONIFIED);
//					UIHeartbeat ui_heartbeat = new UIHeartbeat(ui, 500);
//					ui_heartbeat.start();
//					// running_threads.add(ui_heartbeat);
//					logger.info("Starting UI heartbeat");
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//
//	}
//
//	public static void main(String[] args) throws Exception {
//		new ZHSI();
//	}
	
	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		launch(args);
	}
}
