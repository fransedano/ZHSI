/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.displays.nd;
//
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//
//import javax.imageio.ImageIO;
//
//import org.andreels.zhsi.ZHSIPreferences;
//import org.andreels.zhsi.ZHSIStatus;
//import org.andreels.zhsi.elevationData.ElevationRepository;
//import org.andreels.zhsi.navdata.CoordinateSystem;
//import org.andreels.zhsi.utils.AzimuthalEquidistantProjection;
//import org.andreels.zhsi.utils.Projection;
//import org.andreels.zhsi.xpdata.XPData;
//
//import com.jogamp.opengl.GL2;
//import com.jogamp.opengl.GLAutoDrawable;
//import com.jogamp.opengl.GLEventListener;
//import com.jogamp.opengl.awt.GLJPanel;
//import com.jogamp.opengl.util.awt.AWTGLReadBufferUtil;
//
//
//public class RenderTerrain extends GLJPanel implements GLEventListener {
public class RenderTerrain {
	
}
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private ElevationRepository elevRepository;
//	private ZHSIPreferences preferences;
//	private Projection map_projection;
//	private XPData xpd;
//	GLAutoDrawable drawable;
//	public BufferedImage terrainOne;
//	public BufferedImage terrainTwo;
//	float rotate = 0f;
//	int max_range = 10;
//	float center_lat;
//	float center_lon;
//	float map_center_x = 343f;
//	float map_center_y = 139f;
//	float map_up = 0f;
//	float pixels_per_nm;
//	public float peak_max = 0f;
//	public float peak_min = 8500f;
//	public float ref_alt = 0f;
//	private float terrain_max;
//	private float terrain_min;
//	private float high_band;
//	private float middle_band;
//	private float low_band;
//	private boolean peaks_mode_on;
//	private float scaling_factor;
//	private String pilot;
//	private int imageNumber;
//
//	public RenderTerrain(XPData xpd, String pilot) {
//		this.pilot = pilot;
//		this.xpd = xpd;
//		this.elevRepository = ElevationRepository.get_instance();
//		this.map_projection = new AzimuthalEquidistantProjection(this.pilot);
//		this.preferences = ZHSIPreferences.getInstance();
//		peaks_mode_on = false;
//	}
//
//
//	@Override
//	public void display(GLAutoDrawable drawable) {
//
//		this.center_lat = this.xpd.latitude();
//		this.center_lon = this.xpd.longitude();
//		this.map_projection.setCenter(map_center_x, map_center_y);
//		this.map_projection.setAcf(this.center_lat, this.center_lon);
//		this.map_projection.setScale(pixels_per_nm);
//
//		final GL2 gl = drawable.getGL().getGL2();
//		peak_max = 0f;
//		peak_min= 8500f;
//		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
//		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
//
//		
//		ref_alt = ref_altitude();
//		
//		if(this.xpd.efis_terr_on(pilot) && ZHSIStatus.egpws_db_status.equals(ZHSIStatus.STATUS_EGPWS_DB_LOADED)) {
//
//			float delta_lat = max_range * CoordinateSystem.deg_lat_per_nm();
//			float delta_lon = max_range * CoordinateSystem.deg_lon_per_nm(this.center_lat);
//			
//		
//
//			float lat_max = this.center_lat + delta_lat * 1f;
//			float lat_min = this.center_lat - delta_lat * 1f;
//			float lon_max = this.center_lon + delta_lon * 1f;
//			float lon_min = this.center_lon - delta_lon * 1f;
//
//			float lat_step = (lat_max - lat_min) / 165f;
//			float lon_step = (lon_max - lon_min) / 165f;
//
//
//
//			gl.glPushMatrix();
//			gl.glTranslatef(this.map_center_x, this.map_center_y, 0f);
//			gl.glRotatef(this.map_up , 0f, 0f, this.map_center_x);
//			gl.glTranslatef(-this.map_center_x, -this.map_center_y , 0f);
//			gl.glPointSize(2.5f * scaling_factor);
//			float point = 6f * scaling_factor;
//			int amber_gear = 250;
//			if(this.xpd.gear_up()) {
//				amber_gear = 500;
//			}
//
//			for (float lat=lat_min; lat<= lat_max; lat+=lat_step) {
//				for (float lon=lon_min; lon<=lon_max; lon+=lon_step) {
//					
//				
//					
//					if(CoordinateSystem.isAhead(this.center_lat, this.center_lon, lat, lon, this.xpd.track())) {
//						
//						float elevation = elevRepository.get_elevation(lat, lon) * 3.2808f;
//						//float dist = CoordinateSystem.rough_distance(this.center_lat, this.center_lon, lat, lon);
//						map_projection.setPoint(lat, lon);
//						float x = map_projection.getX();
//						float y = map_projection.getY();
//						peak_min = Math.min(peak_min, elevation);
//						peak_max = Math.max(peak_max, elevation);
//						
//						if(peaks_mode_on) {// 500feet or more above the highest peak - green YQ296
//							
//							if(elevation >= terrain_max - 200) {
//								point = 10f * scaling_factor;
//								gl.glColor3f(0.0f, 1.0f, 0.0f);
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//
//							}else if(elevation >= terrain_max - 2000  && elevation < terrain_max - 200) {
//				
//								point = 6f * scaling_factor;
//								gl.glColor3f(0.0f, 0.80f, 0.0f);
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//
//							}else if(elevation > terrain_max - 2500  && elevation < terrain_max - 2000) {
//					
//								point = 3f * scaling_factor;
//								gl.glColor3f(0.0f, 0.70f, 0.0f);
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							}else {
//								point = 3f * scaling_factor;
//								gl.glColor3f(0.0f, 0.00f, 0.0f);
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							}
//
//
//						}else {
//							
//							if (elevation > ref_alt + 2000) {
//								gl.glColor3f(1.0f, 0.0f, 0.0f); //red
//								point = 6f * scaling_factor;
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							} else if(elevation > ref_alt + 1000 && elevation < ref_alt + 2000) {
//								gl.glColor3f(1.0f, 0.60f, 0.2f); 
//								point = 6f * scaling_factor;
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							} else if(elevation > ref_alt - amber_gear && elevation < ref_alt + 1000) {
//								gl.glColor3f(1.0f, 0.70f, 0.4f); 
//								point = 6f * scaling_factor;
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							} else if(elevation > ref_alt - 2000 && elevation < ref_alt - amber_gear) {
//								gl.glColor3f(0.0f, 0.70f, 0.0f);
//								point = 6f * scaling_factor;
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							}else {
//								gl.glColor3f(0.0f, 0.0f, 0.0f);
//								point = 6f * scaling_factor;
//								gl.glBegin(GL2.GL_TRIANGLES);
//								gl.glVertex2f(x, y);
//								gl.glVertex2f(x + point, y);
//								gl.glVertex2f(x + point, y + point);
//								gl.glEnd();
//							}
//			
////							gl.glBegin(GL2.GL_TRIANGLES);
////							gl.glVertex2f(x, y);
////							gl.glVertex2f(x + point, y);
////							gl.glVertex2f(x + point, y + point);
////							gl.glEnd();
//							
//						}
//
//					}
//					
//				}
//			}
//			
//			terrain_max = peak_max;
//			terrain_min = peak_min;
//			
//			if(terrain_max < (ref_alt -500)) { // 500feet or more above the highest peak - green YQ296
//				peaks_mode_on = true;
//				high_band = terrain_max - 100f;
//				middle_band = terrain_min + (terrain_max-terrain_min)/2;
//				low_band = terrain_min + (terrain_max-terrain_min)/4;
//			}else {
//				peaks_mode_on = false;
//			}
//			gl.glPopMatrix();
//			if(imageNumber == 1) {
//
//				terrainOne = new AWTGLReadBufferUtil(drawable.getGLProfile(), false).readPixelsToBufferedImage(drawable.getGL(), 0, 0, 686, 674, true /* awtOrientation */);
////				try {
////					ImageIO.write(terrainOne, "png", new File("terrainOne.png"));
////				} catch (IOException e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				} 
//			}else {
//				terrainTwo = new AWTGLReadBufferUtil(drawable.getGLProfile(), false).readPixelsToBufferedImage(drawable.getGL(), 0, 0, 686, 674, true /* awtOrientation */);	
//			}
//
//		}
//		gl.glFlush();
//
//	}
//
//	@Override
//	public void dispose(GLAutoDrawable drawable) {
//		final GL2 gl = drawable.getGL().getGL2();
//
//	}
//
//	@Override
//	public void init(GLAutoDrawable drawable) {
//		final GL2 gl = drawable.getGL().getGL2();
//		this.drawable = drawable;
//
//	}
//
//	@Override
//	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
//		final GL2 gl = drawable.getGL().getGL2();
//		gl.glViewport(0, 0, width, height);
//		gl.glMatrixMode(GL2.GL_PROJECTION);
//		gl.glLoadIdentity();
//		gl.glOrthof(0.0f, width, height, 0.0f, 0.0f, 1.0f);	
//
//	}
//
//	public void render(float pixels_per_nm, int max_range, float map_up, float map_center_x, float map_center_y, float scaling_factor, int imageNumber) {
//		this.pixels_per_nm = pixels_per_nm;
//		this.max_range = max_range;
//		this.map_up = map_up;
//		this.map_center_x = map_center_x;
//		this.map_center_y = map_center_y;
//		this.scaling_factor = scaling_factor;
//		this.imageNumber = imageNumber;
//		display(drawable);
//	}
//
//	public float ref_altitude() {
//		float alt = this.xpd.altitude("cpt");
//		float vvi = this.xpd.vvi("cpt");
//		return (vvi < -1000 ? alt + vvi/2 : alt);
//	}
//
//
//}
