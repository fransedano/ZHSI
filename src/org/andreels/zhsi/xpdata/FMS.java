/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;

import java.util.Base64;

import org.andreels.zhsi.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zhsi.ExtPlaneInterface.data.DataRef;
import org.andreels.zhsi.ExtPlaneInterface.util.Observer;

public class FMS extends BaseDataClass {
	
	Observer<DataRef> fms_data;
	Observer<DataRef> active_route;
	Observer<DataRef> modified_route;
	Observer<DataRef> holds;
	
	
	// fms_data
	private final String ANP = "laminar/B738/fms/anp";
	private final String RNP = "laminar/B738/fms/rnp";
	private final String TRANS_ALT = "laminar/B738/FMS/fmc_trans_alt";
	private final String[] FPLN_ACTIVE = {"laminar/B738/fms/fpln_acive","laminar/B738/fms/fpln_acive_fo"};
	private final String FPLN_NAV_ID = "laminar/B738/fms/fpln_nav_id:string"; //active waypoint name to be displayed top right only when above is true
	private final String FPLN_NAV_ID_ETA = "laminar/B738/fms/id_eta:string"; // eta to active waypoint in zulu format 0648.1
	//private final String FPLN_NAV_ID_ALT = "laminar/B738/fms/calc_wpt_alt"; // alt for next waypoint
	private final String FPLN_NAV_ID_DIST = "laminar/B738/fms/lnav_dist_next"; //distance in NM to next waypoint
	private final String FMS_VREF = "laminar/B738/FMS/vref";
	private final String FMS_VR_SET = "laminar/B738/FMS/vr_set";
	private final String FMS_V1_SET = "laminar/B738/FMS/v1_set";
	private final String FMS_V2_15 = "laminar/B738/FMS/v2_15";
	private final String FMS_VREF_15 = "laminar/B738/FMS/vref_15";
	private final String FMS_VREF_25 = "laminar/B738/FMS/vref_25";
	private final String FMS_VREF_30 = "laminar/B738/FMS/vref_30";
	private final String FMS_VREF_40 = "laminar/B738/FMS/vref_40";
	private final String FMS_VREF_BUGS = "laminar/B738/FMS/vref_bugs";
	private final String FMS_LEGS_NUM = "laminar/B738/vnav/legs_num";
	private final String FMS_LEGS_NUM2 = "laminar/B738/fms/legs_num2";
	private final String LEGS_MOD_ACTIVE = "laminar/B738/fms/legs_mod_active";
	private final String[] LEGS_STEP_CTR_IDX = {"laminar/B738/fms/legs_step_ctr", "laminar/B738/fms/legs_step_ctr_fo"};
	private final String MISSED_APP_WPT_IDX = "laminar/B738/fms/missed_app_wpt_idx";
	private final String MISSED_APP_ACTIVE = "laminar/B738/fms/missed_app_act";
	private final String[] ILS_ROTATE0 = {"laminar/B738/pfd/ils_rotate0", "laminar/B738/pfd/ils_fo_rotate0"}; // dep runway
	private final String[] ILS_SHOW0 = {"laminar/B738/pfd/ils_show0", "laminar/B738/pfd/ils_fo_show0"};
	private final String[] ILS_ROTATE = {"laminar/B738/pfd/ils_rotate", "laminar/B738/pfd/ils_fo_rotate"}; // dest runway
	private final String[] ILS_SHOW = {"laminar/B738/pfd/ils_show", "laminar/B738/pfd/ils_fo_show"};
	private final String VNAV_IDX = "laminar/B738/fms/vnav_idx";
	private final String INTDIR_ACT = "laminar/B738/fms/intdir_act";
	private final String FMS_TRACK = "laminar/B738/fms/gps_track_degtm";
	private final String FMS_COURSE = "laminar/B738/fms/gps_course_degtm";
	private final String FMS_XTRACK = "laminar/B738/fms/xtrack";
	private final String RTE_EDIT_ROT_ACT = "laminar/B738/nd/rte_edit_rot_act"; // (this dref - 180 + heading) % 360
	private final String APPROACH_FLAPS = "laminar/B738/FMS/approach_flaps";
	private final String APPROACH_SPEED = "laminar/B738/FMS/approach_speed";
	
	public float anp;
	public float rnp;
	public int trans_alt;
	public int[] fpln_active = new int[2];
	public String fpln_nav_id;
	public String active_waypoint;
	public String fpln_nav_id_eta;
	//public int fpln_nav_id_alt;
	public float fpln_nav_id_dist;
	public float fms_vref = 0f;
	public float fms_vr_set = 0f;
	public float fms_v1_set = 0f;
	public float fms_v2_15 = 0f;
	public float fms_vref_15 = 0f;
	public float fms_vref_25 = 0f;
	public float fms_vref_30 = 0f;
	public float fms_vref_40 = 0f;
	public int fms_vref_bugs = 0;
	public int fms_legs_num2 = 0;
	public int fms_legs_num = 0;
	public int legs_mod_active = 0;
	public int[] legs_step_ctr_idx = new int[2];
	public float[] ils_rotate0 = new float[2];
	public int[] ils_show0 = new int[2];
	public float[] ils_rotate = new float[2];
	public int[] ils_show = new int[2];
	public int missed_app_wpt_idx = 0;
	public int missed_app_active = 0;
	public int vnav_idx = 0;
	public int intdir_act = 0;
	public float approach_flaps = 0f;
	public float approach_speed = 0f;
	public float fms_track = 0f;
	public float fms_course = 0f;
	public float fms_xtrack = 0f;
	public float rte_edit_rot_act = 0f;
	
	//fms active route
	
	private final String LEGS = "laminar/B738/fms/legs"; ///only active flight plan (string, space separated)
	private final String LEGS_LAT = "laminar/B738/fms/legs_lat"; //only active flight plan [128]
	private final String LEGS_LON = "laminar/B738/fms/legs_lon"; //only active flight plan [128]
	private final String LEGS_ALT_CALC = "laminar/B738/fms/legs_alt_calc"; //[128] altitude for legs
	private final String LEGS_ALT_REST1 = "laminar/B738/fms/legs_alt_rest1";
	private final String LEGS_ALT_REST2 = "laminar/B738/fms/legs_alt_rest2";
	private final String LEGS_ALT_REST_TYPE = "laminar/B738/fms/legs_alt_rest_type"; // 1 =below, 2 = above, 3 = between (block), 4 = dest runway
	private final String LEGS_TYPE = "laminar/B738/fms/legs_type"; // 0 waypoint, 5 is flyover waypoint
	@SuppressWarnings("unused")
	private final String NUM_OF_WPTS = "laminar/B738/fms/num_of_wpts";

	public String origin;
	public String dest;
	
	public String[] waypoints;
	public float[] legs_lat = new float[128];
	public float[] legs_lon = new float[128];
	public float[] legs_alt_calc = new float[128];
	public float[] legs_alt_rest1 = new float[128];
	public float[] legs_alt_rest2 = new float[128];
	public float[] legs_alt_rest_type = new float[128];
	public float[] legs_type = new float[128];
	public int num_of_wpts = 0;

	
	//fms modified route
	
	private final String LEGS_2 = "laminar/B738/fms/legs_2";
	private final String LEGS_LAT_2 = "laminar/B738/fms/legs_lat_2"; //only active flight plan [128]
	private final String LEGS_LON_2 = "laminar/B738/fms/legs_lon_2"; //only active flight plan [128]
	private final String LEGS_ALT_CALC_2 = "laminar/B738/fms/legs_alt_calc_2"; //[128] altitude for legs
	private final String LEGS_ALT_REST1_2 = "laminar/B738/fms/legs_alt_rest1_2";
	private final String LEGS_ALT_REST2_2 = "laminar/B738/fms/legs_alt_rest2_2";
	private final String LEGS_ALT_REST_TYPE_2 = "laminar/B738/fms/legs_alt_rest_type_2"; // 1 =below, 2 = above, 3 = between (block), 4 = dest runway
	private final String LEGS_TYPE_2 = "laminar/B738/fms/legs_type_2"; // 0 waypoint, 5 is flyover waypoint
	@SuppressWarnings("unused")
	private final String NUM_OF_WPTS_2 = "laminar/B738/fms/num_of_wpts_2";
	
	
	public String[] waypoints_2;
	public float[] legs_lat_2 = new float[128];
	public float[] legs_lon_2 = new float[128];
	public float[] legs_alt_calc_2 = new float[128];
	public float[] legs_alt_rest1_2 = new float[128];
	public float[] legs_alt_rest2_2 = new float[128];
	public float[] legs_alt_rest_type_2 = new float[128];
	public float[] legs_type_2 = new float[128];
	public int num_of_wpts_2 = 0;
	
	
	private final String[] HOLD_TYPE = {"laminar/B738/nd/hold_type", "laminar/B738/nd/hold_fo_type"};
	private final String[] HOLD_DIST = {"laminar/B738/nd/hold_dist", "laminar/B738/nd/hold_fo_dist"};
	private final String[] HOLD_CRS = {"laminar/B738/nd/hold_crs", "laminar/B738/nd/hold_fo_crs"};
	private final String[] HOLD_X = {"laminar/B738/nd/hold_x", "laminar/B738/nd/hold_fo_x"};
	private final String[] HOLD_Y = {"laminar/B738/nd/hold_y", "laminar/B738/nd/hold_fo_y"};
	
	public float[][] hold_type = new float[2][5];
	public float[][] hold_dist = new float[2][5];
	public float[][] hold_crs = new float[2][5];
	public float[][] hold_x = new float[2][5];
	public float[][] hold_y = new float[2][5];
	
	
	
	public FMS(ExtPlaneInterface iface) {
		super(iface);
		
		drefs.add(APPROACH_SPEED);
		drefs.add(APPROACH_FLAPS);
		drefs.add(FMS_COURSE);
		drefs.add(FMS_TRACK);
		drefs.add(FMS_XTRACK);
		drefs.add(RTE_EDIT_ROT_ACT);
		
		// fms_data
		fms_data = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				switch (object.getName()) {
				case ANP:
					anp = Float.parseFloat(object.getValue()[0]);
					break;
				case RNP:
					rnp = Float.parseFloat(object.getValue()[0]);
					break;
				case TRANS_ALT:
					trans_alt = Integer.parseInt(object.getValue()[0]);
					break;
				case FPLN_NAV_ID:
					active_waypoint = object.getValue()[0].replaceAll("\"", "");
					break;
				case FMS_VREF:
					fms_vref = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_15:
					fms_vref_15 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_25:
					fms_vref_25 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_30:
					fms_vref_30 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_40:
					fms_vref_40 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_BUGS:
					fms_vref_bugs = Integer.parseInt(object.getValue()[0]);
					break;
				case FPLN_NAV_ID_ETA:
					fpln_nav_id_eta = object.getValue()[0].replaceAll("\"", "");
					break;
				case FPLN_NAV_ID_DIST:
					fpln_nav_id_dist = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VR_SET:
					fms_vr_set = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_V1_SET:
					fms_v1_set = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_V2_15:
					fms_v2_15 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_LEGS_NUM2:
					fms_legs_num2 = Integer.parseInt(object.getValue()[0]);
					break;
				case FMS_LEGS_NUM:
					fms_legs_num = Integer.parseInt(object.getValue()[0]);
					break;
				case LEGS_MOD_ACTIVE:
					legs_mod_active = Integer.parseInt(object.getValue()[0]);
					break;
				case MISSED_APP_WPT_IDX:
					missed_app_wpt_idx = Integer.parseInt(object.getValue()[0]);
					break;
				case MISSED_APP_ACTIVE:
					missed_app_active = Integer.parseInt(object.getValue()[0]);
					break;
				case VNAV_IDX:
					vnav_idx = Integer.parseInt(object.getValue()[0]);
					break;
				case INTDIR_ACT:
					intdir_act = Integer.parseInt(object.getValue()[0]);
					break;
				case APPROACH_SPEED:
					approach_speed = Float.parseFloat(object.getValue()[0]);
					break;
				case APPROACH_FLAPS:
					approach_flaps = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_COURSE:
					fms_course = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_TRACK:
					fms_track = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_XTRACK:
					fms_xtrack = Float.parseFloat(object.getValue()[0]);
					break;
				case RTE_EDIT_ROT_ACT:
					rte_edit_rot_act = Float.parseFloat(object.getValue()[0]);
					break;
				}
				
				for(int i = 0; i < 2; i++) {
					if(object.getName().equals(LEGS_STEP_CTR_IDX[i])) {
						legs_step_ctr_idx[i] = Integer.parseInt(object.getValue()[0]);
					}
					if(object.getName().equals(FPLN_ACTIVE[i])) {
						fpln_active[i] = Integer.parseInt(object.getValue()[0]);
					}
					if(object.getName().equals(ILS_ROTATE0[i])) {
						ils_rotate0[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(ILS_SHOW0[i])) {
						ils_show0[i] = Integer.parseInt(object.getValue()[0]);
					}
					if(object.getName().equals(ILS_ROTATE[i])) {
						ils_rotate[i] = Float.parseFloat(object.getValue()[0]);
					}
					if(object.getName().equals(ILS_SHOW[i])) {
						ils_show[i] = Integer.parseInt(object.getValue()[0]);
					}
				}
			}	
		};
		
		//fms active route
		active_route = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				switch (object.getName()) {
				case LEGS:
					byte[] legsByte = Base64.getDecoder().decode(object.getValue()[0]);
					String decodedString = new String(legsByte);
					waypoints = decodedString.split("\\s+");
					num_of_wpts = waypoints.length;
					origin = waypoints[0];
					dest = waypoints[waypoints.length - 1];
					break;
				case LEGS_LAT:
					for(int i = 0; i < 128; i++) {
						legs_lat[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_LON:
					for(int i = 0; i < 128; i++) {
						legs_lon[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_CALC:
					for(int i = 0; i < 128; i++) {
						legs_alt_calc[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_REST1:
					for(int i = 0; i < 128; i++) {
						legs_alt_rest1[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_REST2:
					for(int i = 0; i < 128; i++) {
						legs_alt_rest2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_REST_TYPE:
					for(int i = 0; i < 128; i++) {
						legs_alt_rest_type[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_TYPE:
					for(int i = 0; i < 128; i++) {
						legs_type[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				}
			}		
		};
		
		//fms modified route
		modified_route = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				switch (object.getName()) {
				case LEGS_2:
					byte[] legsByte = Base64.getDecoder().decode(object.getValue()[0]);
					String decodedString = new String(legsByte);
					waypoints_2 = decodedString.split("\\s+");
					num_of_wpts_2 = waypoints_2.length;
					//origin = waypoints_2[0];
					//dest = waypoints_2[waypoints_2.length - 1];
					break;
				case LEGS_LAT_2:
					for(int i = 0; i < 128; i++) {
						legs_lat_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_LON_2:
					for(int i = 0; i < 128; i++) {
						legs_lon_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_CALC_2:
					for(int i = 0; i < 128; i++) {
						legs_alt_calc_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_REST1_2:
					for(int i = 0; i < 128; i++) {
						legs_alt_rest1_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_REST2_2:
					for(int i = 0; i < 128; i++) {
						legs_alt_rest2_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_ALT_REST_TYPE_2:
					for(int i = 0; i < 128; i++) {
						legs_alt_rest_type_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				case LEGS_TYPE_2:
					for(int i = 0; i < 128; i++) {
						legs_type_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					break;
				}
				
			}
		};
		
		holds = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				for(int i = 0; i < 2; i++) {
					
					if(object.getName().equals(HOLD_TYPE[i])) {
						for(int j = 0; j < 5; j++) {
							hold_type[i][j] = Float.parseFloat(object.getValue()[j]);
						}
					}
					if(object.getName().equals(HOLD_DIST[i])) {
						for(int j = 0; j < 5; j++) {
							hold_dist[i][j] = Float.parseFloat(object.getValue()[j]);
						}
					}
					if(object.getName().equals(HOLD_CRS[i])) {
						for(int j = 0; j < 5; j++) {
							hold_crs[i][j] = Float.parseFloat(object.getValue()[j]);
						}
					}
					if(object.getName().equals(HOLD_X[i])) {
						for(int j = 0; j < 5; j++) {
							hold_x[i][j] = Float.parseFloat(object.getValue()[j]);
						}
					}
					if(object.getName().equals(HOLD_Y[i])) {
						for(int j = 0; j < 5; j++) {
							hold_y[i][j] = Float.parseFloat(object.getValue()[j]);
						}
					}
				}
				
			}
			
		};
	}

	@Override
	public void subscribeDrefs() {
		


	}

	@Override
	public void includeDrefs() {
		
		for(String dref : drefs) {
			iface.includeDataRef(dref);
			iface.observeDataRef(dref, fms_data);
		}
		
		iface.includeDataRef(ANP, 0.001f);
		iface.includeDataRef(RNP, 0.001f);
		iface.includeDataRef(TRANS_ALT);
		iface.includeDataRef(FPLN_NAV_ID);
		iface.includeDataRef(FMS_VREF);
		iface.includeDataRef(FMS_VREF_15);
		iface.includeDataRef(FMS_VREF_25);
		iface.includeDataRef(FMS_VREF_30);
		iface.includeDataRef(FMS_VREF_40);
		iface.includeDataRef(FMS_VREF_BUGS);
		iface.includeDataRef(FPLN_NAV_ID_ETA);
		iface.includeDataRef(FPLN_NAV_ID_DIST);
		iface.includeDataRef(FMS_VR_SET);
		iface.includeDataRef(FMS_V1_SET);
		iface.includeDataRef(FMS_V2_15);
		iface.includeDataRef(FMS_LEGS_NUM2);
		iface.includeDataRef(FMS_LEGS_NUM);
		iface.includeDataRef(LEGS_MOD_ACTIVE);
		iface.includeDataRef(MISSED_APP_WPT_IDX);
		iface.includeDataRef(MISSED_APP_ACTIVE);
		iface.includeDataRef(VNAV_IDX);
		iface.includeDataRef(INTDIR_ACT);
		//
		iface.observeDataRef(ANP, fms_data);
		iface.observeDataRef(RNP, fms_data);
		iface.observeDataRef(TRANS_ALT, fms_data);
		iface.observeDataRef(FPLN_NAV_ID, fms_data);
		iface.observeDataRef(FMS_VREF, fms_data);
		iface.observeDataRef(FMS_VREF_15, fms_data);
		iface.observeDataRef(FMS_VREF_25, fms_data);
		iface.observeDataRef(FMS_VREF_30, fms_data);
		iface.observeDataRef(FMS_VREF_40, fms_data);
		iface.observeDataRef(FMS_VREF_BUGS, fms_data);
		iface.observeDataRef(FPLN_NAV_ID_ETA, fms_data);
		iface.observeDataRef(FPLN_NAV_ID_DIST, fms_data);
		iface.observeDataRef(FMS_VR_SET, fms_data);
		iface.observeDataRef(FMS_V1_SET, fms_data);
		iface.observeDataRef(FMS_V2_15, fms_data);
		iface.observeDataRef(FMS_LEGS_NUM2, fms_data);
		iface.observeDataRef(FMS_LEGS_NUM, fms_data);
		iface.observeDataRef(LEGS_MOD_ACTIVE, fms_data);
		iface.observeDataRef(MISSED_APP_WPT_IDX, fms_data);
		iface.observeDataRef(MISSED_APP_ACTIVE, fms_data);
		iface.observeDataRef(VNAV_IDX, fms_data);
		iface.observeDataRef(INTDIR_ACT, fms_data);
		
		for(int i = 0; i < 2; i++) {
			iface.includeDataRef(FPLN_ACTIVE[i]);
			iface.observeDataRef(FPLN_ACTIVE[i], fms_data);
			iface.includeDataRef(ILS_ROTATE0[i]);
			iface.observeDataRef(ILS_ROTATE0[i], fms_data);
			iface.includeDataRef(ILS_ROTATE[i]);
			iface.observeDataRef(ILS_ROTATE[i], fms_data);
			iface.includeDataRef(ILS_SHOW0[i]);
			iface.observeDataRef(ILS_SHOW0[i], fms_data);
			iface.includeDataRef(ILS_SHOW[i]);
			iface.observeDataRef(ILS_SHOW[i], fms_data);
			iface.includeDataRef(LEGS_STEP_CTR_IDX[i]);
			iface.observeDataRef(LEGS_STEP_CTR_IDX[i], fms_data);
			iface.includeDataRef(HOLD_TYPE[i]);
			iface.includeDataRef(HOLD_DIST[i]);
			iface.includeDataRef(HOLD_CRS[i]);
			iface.includeDataRef(HOLD_X[i], 0.01f);
			iface.includeDataRef(HOLD_Y[i], 0.01f);
			iface.observeDataRef(HOLD_TYPE[i], holds);
			iface.observeDataRef(HOLD_DIST[i], holds);
			iface.observeDataRef(HOLD_CRS[i], holds);
			iface.observeDataRef(HOLD_X[i], holds);
			iface.observeDataRef(HOLD_Y[i], holds);
		}
		
		
		iface.includeDataRef(LEGS);
		iface.includeDataRef(LEGS_LAT);
		iface.includeDataRef(LEGS_LON);
		iface.includeDataRef(LEGS_ALT_CALC);
		iface.includeDataRef(LEGS_ALT_REST1);
		iface.includeDataRef(LEGS_ALT_REST2);
		iface.includeDataRef(LEGS_ALT_REST_TYPE);
		iface.includeDataRef(LEGS_TYPE);

		iface.observeDataRef(LEGS, active_route);
		iface.observeDataRef(LEGS_LAT, active_route);
		iface.observeDataRef(LEGS_LON, active_route);
		iface.observeDataRef(LEGS_ALT_CALC, active_route);
		iface.observeDataRef(LEGS_ALT_REST1, active_route);
		iface.observeDataRef(LEGS_ALT_REST2, active_route);
		iface.observeDataRef(LEGS_ALT_REST_TYPE, active_route);
		iface.observeDataRef(LEGS_TYPE, active_route);

		iface.includeDataRef(LEGS_2);
		iface.includeDataRef(LEGS_LAT_2);
		iface.includeDataRef(LEGS_LON_2);
		iface.includeDataRef(LEGS_ALT_CALC_2);
		iface.includeDataRef(LEGS_ALT_REST1_2);
		iface.includeDataRef(LEGS_ALT_REST2_2);
		iface.includeDataRef(LEGS_ALT_REST_TYPE_2);
		iface.includeDataRef(LEGS_TYPE_2);

		iface.observeDataRef(LEGS_2, modified_route);
		iface.observeDataRef(LEGS_LAT_2, modified_route);
		iface.observeDataRef(LEGS_LON_2, modified_route);
		iface.observeDataRef(LEGS_ALT_CALC_2, modified_route);
		iface.observeDataRef(LEGS_ALT_REST1_2, modified_route);
		iface.observeDataRef(LEGS_ALT_REST2_2, modified_route);
		iface.observeDataRef(LEGS_ALT_REST_TYPE_2, modified_route);
		iface.observeDataRef(LEGS_TYPE_2, modified_route);

		
	}

	@Override
	public void excludeDrefs() {
		
		for(String dref : drefs) {
			iface.excludeDataRef(dref);
			iface.unObserveDataRef(dref, fms_data);
		}	
		
		iface.excludeDataRef(ANP);
		iface.excludeDataRef(RNP);
		iface.excludeDataRef(TRANS_ALT);
		iface.excludeDataRef(FPLN_NAV_ID);
		iface.excludeDataRef(FMS_VREF);
		iface.excludeDataRef(FMS_VREF_15);
		iface.excludeDataRef(FMS_VREF_25);
		iface.excludeDataRef(FMS_VREF_30);
		iface.excludeDataRef(FMS_VREF_40);
		iface.excludeDataRef(FMS_VREF_BUGS);
		iface.excludeDataRef(FPLN_NAV_ID_ETA);
		iface.excludeDataRef(FPLN_NAV_ID_DIST);
		iface.excludeDataRef(FMS_VR_SET);
		iface.excludeDataRef(FMS_V1_SET);
		iface.excludeDataRef(FMS_V2_15);
		iface.excludeDataRef(FMS_LEGS_NUM2);
		iface.excludeDataRef(FMS_LEGS_NUM);
		iface.excludeDataRef(LEGS_MOD_ACTIVE);
		iface.excludeDataRef(MISSED_APP_WPT_IDX);
		iface.excludeDataRef(MISSED_APP_ACTIVE);
		iface.excludeDataRef(VNAV_IDX);
		iface.excludeDataRef(INTDIR_ACT);
		//
		iface.unObserveDataRef(ANP, fms_data);
		iface.unObserveDataRef(RNP, fms_data);
		iface.unObserveDataRef(TRANS_ALT, fms_data);
		iface.unObserveDataRef(FPLN_NAV_ID, fms_data);
		iface.unObserveDataRef(FMS_VREF, fms_data);
		iface.unObserveDataRef(FMS_VREF_15, fms_data);
		iface.unObserveDataRef(FMS_VREF_25, fms_data);
		iface.unObserveDataRef(FMS_VREF_30, fms_data);
		iface.unObserveDataRef(FMS_VREF_40, fms_data);
		iface.unObserveDataRef(FMS_VREF_BUGS, fms_data);
		iface.unObserveDataRef(FPLN_NAV_ID_ETA, fms_data);
		iface.unObserveDataRef(FPLN_NAV_ID_DIST, fms_data);
		iface.unObserveDataRef(FMS_VR_SET, fms_data);
		iface.unObserveDataRef(FMS_V1_SET, fms_data);
		iface.unObserveDataRef(FMS_V2_15, fms_data);
		iface.unObserveDataRef(FMS_LEGS_NUM2, fms_data);
		iface.unObserveDataRef(FMS_LEGS_NUM, fms_data);
		iface.unObserveDataRef(LEGS_MOD_ACTIVE, fms_data);
		iface.unObserveDataRef(MISSED_APP_WPT_IDX, fms_data);
		iface.unObserveDataRef(MISSED_APP_ACTIVE, fms_data);
		iface.unObserveDataRef(VNAV_IDX, fms_data);
		iface.unObserveDataRef(INTDIR_ACT, fms_data);
		
		for(int i = 0; i < 2; i++) {
			iface.excludeDataRef(FPLN_ACTIVE[i]);
			iface.unObserveDataRef(FPLN_ACTIVE[i], fms_data);
			iface.excludeDataRef(ILS_ROTATE0[i]);
			iface.unObserveDataRef(ILS_ROTATE0[i], fms_data);
			iface.excludeDataRef(ILS_ROTATE[i]);
			iface.unObserveDataRef(ILS_ROTATE[i], fms_data);
			iface.excludeDataRef(ILS_SHOW0[i]);
			iface.unObserveDataRef(ILS_SHOW0[i], fms_data);
			iface.excludeDataRef(ILS_SHOW[i]);
			iface.unObserveDataRef(ILS_SHOW[i], fms_data);
			iface.excludeDataRef(LEGS_STEP_CTR_IDX[i]);
			iface.unObserveDataRef(LEGS_STEP_CTR_IDX[i], fms_data);
			iface.excludeDataRef(HOLD_TYPE[i]);
			iface.excludeDataRef(HOLD_DIST[i]);
			iface.excludeDataRef(HOLD_CRS[i]);
			iface.excludeDataRef(HOLD_X[i]);
			iface.excludeDataRef(HOLD_Y[i]);
			iface.unObserveDataRef(HOLD_TYPE[i], holds);
			iface.unObserveDataRef(HOLD_DIST[i], holds);
			iface.unObserveDataRef(HOLD_CRS[i], holds);
			iface.unObserveDataRef(HOLD_X[i], holds);
			iface.unObserveDataRef(HOLD_Y[i], holds);
		}
		
		
		iface.excludeDataRef(LEGS);
		iface.excludeDataRef(LEGS_LAT);
		iface.excludeDataRef(LEGS_LON);
		iface.excludeDataRef(LEGS_ALT_CALC);
		iface.excludeDataRef(LEGS_ALT_REST1);
		iface.excludeDataRef(LEGS_ALT_REST2);
		iface.excludeDataRef(LEGS_ALT_REST_TYPE);
		iface.excludeDataRef(LEGS_TYPE);

		iface.unObserveDataRef(LEGS, active_route);
		iface.unObserveDataRef(LEGS_LAT, active_route);
		iface.unObserveDataRef(LEGS_LON, active_route);
		iface.unObserveDataRef(LEGS_ALT_CALC, active_route);
		iface.unObserveDataRef(LEGS_ALT_REST1, active_route);
		iface.unObserveDataRef(LEGS_ALT_REST2, active_route);
		iface.unObserveDataRef(LEGS_ALT_REST_TYPE, active_route);
		iface.unObserveDataRef(LEGS_TYPE, active_route);

		iface.excludeDataRef(LEGS_2);
		iface.excludeDataRef(LEGS_LAT_2);
		iface.excludeDataRef(LEGS_LON_2);
		iface.excludeDataRef(LEGS_ALT_CALC_2);
		iface.excludeDataRef(LEGS_ALT_REST1_2);
		iface.excludeDataRef(LEGS_ALT_REST2_2);
		iface.excludeDataRef(LEGS_ALT_REST_TYPE_2);
		iface.excludeDataRef(LEGS_TYPE_2);

		iface.unObserveDataRef(LEGS_2, modified_route);
		iface.unObserveDataRef(LEGS_LAT_2, modified_route);
		iface.unObserveDataRef(LEGS_LON_2, modified_route);
		iface.unObserveDataRef(LEGS_ALT_CALC_2, modified_route);
		iface.unObserveDataRef(LEGS_ALT_REST1_2, modified_route);
		iface.unObserveDataRef(LEGS_ALT_REST2_2, modified_route);
		iface.unObserveDataRef(LEGS_ALT_REST_TYPE_2, modified_route);
		iface.unObserveDataRef(LEGS_TYPE_2, modified_route);
		
	}
}
