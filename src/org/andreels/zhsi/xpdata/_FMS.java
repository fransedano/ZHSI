/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;


import java.util.Base64;


import org.andreels.zhsi.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zhsi.ExtPlaneInterface.data.DataRef;
import org.andreels.zhsi.ExtPlaneInterface.util.Observer;
import org.andreels.zhsi.fms.ActiveLegs;
import org.andreels.zhsi.fms.ActiveWayPoint;
import org.andreels.zhsi.fms.ModifiedLegs;
import org.andreels.zhsi.fms.ModifiedWayPoint;


public class _FMS {

	ExtPlaneInterface iface;

	public static ActiveWayPoint active_entries[];
	public static ModifiedWayPoint modified_entries[];
	public static ActiveLegs active_legs[];
	public static ModifiedLegs mod_legs[];

	// fms_data
	private final String ANP = "laminar/B738/fms/anp";
	private final String RNP = "laminar/B738/fms/rnp";
	private final String TRANS_ALT = "laminar/B738/FMS/fmc_trans_alt";
	private final String[] FPLN_ACTIVE = {"laminar/B738/fms/fpln_acive","laminar/B738/fms/fpln_acive_fo"};
	private final String FPLN_NAV_ID = "laminar/B738/fms/fpln_nav_id:string"; //active waypoint name to be displayed top right only when above is true
	private final String FPLN_NAV_ID_ETA = "laminar/B738/fms/id_eta:string"; // eta to active waypoint in zulu format 0648.1
	//private final String FPLN_NAV_ID_ALT = "laminar/B738/fms/calc_wpt_alt"; // alt for next waypoint
	private final String FPLN_NAV_ID_DIST = "laminar/B738/fms/lnav_dist_next"; //distance in NM to next waypoint
	private final String FMS_VREF = "laminar/B738/FMS/vref";
	private final String FMS_VR_SET = "laminar/B738/FMS/vr_set";
	private final String FMS_V1_SET = "laminar/B738/FMS/v1_set";
	private final String FMS_V2_15 = "laminar/B738/FMS/v2_15";
	private final String FMS_VREF_15 = "laminar/B738/FMS/vref_15";
	private final String FMS_VREF_25 = "laminar/B738/FMS/vref_25";
	private final String FMS_VREF_30 = "laminar/B738/FMS/vref_30";
	private final String FMS_VREF_40 = "laminar/B738/FMS/vref_40";
	private final String FMS_VREF_BUGS = "laminar/B738/FMS/vref_bugs";
	private final String FMS_LEGS_NUM = "laminar/B738/vnav/legs_num";
	private final String FMS_LEGS_NUM2 = "laminar/B738/fms/legs_num2";
	private final String LEGS_MOD_ACTIVE = "laminar/B738/fms/legs_mod_active";
	private final String[] LEGS_STEP_CTR_IDX = {"laminar/B738/fms/legs_step_ctr", "laminar/B738/fms/legs_step_ctr_fo"};
	private final String MISSED_APP_WPT_IDX = "laminar/B738/fms/missed_app_wpt_idx";
	private final String MISSED_APP_ACTIVE = "laminar/B738/fms/missed_app_act";
	private final String[] ILS_ROTATE0 = {"laminar/B738/pfd/ils_rotate0", "laminar/B738/pfd/ils_fo_rotate0"};

	public float anp;
	public float rnp;
	public int trans_alt;
	public int[] fpln_active = new int[2];
	public String fpln_nav_id;
	public String fpln_nav_id_eta;
	//public int fpln_nav_id_alt;
	public float fpln_nav_id_dist;
	public float fms_vref = 0f;
	public float fms_vr_set = 0f;
	public float fms_v1_set = 0f;
	public float fms_v2_15 = 0f;
	public float fms_vref_15 = 0f;
	public float fms_vref_25 = 0f;
	public float fms_vref_30 = 0f;
	public float fms_vref_40 = 0f;
	public int fms_vref_bugs = 0;
	public int fms_legs_num2 = 0;
	public int fms_legs_num = 0;
	public int legs_mod_active = 0;
	public int[] legs_step_ctr_idx = new int[2];
	public float[] ils_rotate0 = new float[2];
	public int missed_app_wpt_idx = 0;
	public int missed_app_active = 0;

	// active fms_waypoints

	private final String LEGS = "laminar/B738/fms/legs"; ///only active flight plan (string, space separated)
	private final String LEGS_2 = "laminar/B738/fms/legs_2";
	private final String LEGS_LAT = "laminar/B738/fms/legs_lat"; //only active flight plan [128]
	private final String LEGS_LON = "laminar/B738/fms/legs_lon"; //only active flight plan [128]
	private final String LEGS_LAT_2 = "laminar/B738/fms/legs_lat_2"; //only active flight plan [128]
	private final String LEGS_LON_2 = "laminar/B738/fms/legs_lon_2"; //only active flight plan [128]
	private final String LEGS_ALT_REST1 = "laminar/B738/fms/legs_alt_rest1";
	private final String LEGS_ALT_REST2 = "laminar/B738/fms/legs_alt_rest2";
	private final String LEGS_ALT_REST1_2 = "laminar/B738/fms/legs_alt_rest1_2";
	private final String LEGS_ALT_REST2_2 = "laminar/B738/fms/legs_alt_rest2_2";
	private final String LEGS_ALT_REST_TYPE1 = "laminar/B738/fms/legs_alt_rest_type"; // 1 =below, 2 = above, 3 = between (block), 4 = dest runway
	private final String LEGS_ALT_REST_TYPE2 = "laminar/B738/fms/legs_alt_rest_type_2"; // 1 =below, 2 = above, 3 = between (block), 4 = dest runway
	private final String LEGS_TYPE = "laminar/B738/fms/legs_type"; // 0 waypoint, 5 is flyover waypoint
	private final String LEGS_TYPE_2 = "laminar/B738/fms/legs_type_2"; // 0 waypoint, 5 is flyover waypoint
	private final String LEGS_ALT_CALC = "laminar/B738/fms/legs_alt_calc"; //[128] altitude for legs
	
	public String[] waypoints;
	public String[] waypoints_2;
	public String origin;
	public String dest;
	public String active_waypoint;
	public float[] legs_lat = new float[128];
	public float[] legs_lon = new float[128];
	public float[] legs_lat_2 = new float[128];
	public float[] legs_lon_2 = new float[128];
	public float[] legs_type = new float[128];
	public float[] legs_type_2 = new float[128];
	public float[] legs_alt_rest1 = new float[128];
	public float[] legs_alt_rest2 = new float[128];
	public float[] legs_alt_rest1_2 = new float[128];
	public float[] legs_alt_rest_type1 = new float[128];
	public float[] legs_alt_rest_type2 = new float[128];


	public int vnav_idx;
	
	//laminar/B738/fms/legs_alt_rest1 //alt restriction
	//laminar/B738/fms/legs_alt_rest1_2
	//laminar/B738/fms/legs_alt_rest2
	//laminar/B738/fms/legs_alt_rest2_2
	//laminar/B738/fms/num_of_wpts, has the number of active waypoints
	//laminar/B738/fms/num_of_wpts_2 number of mod waypoints



	//private final String VNAV_IDX = "laminar/B738/fms/vnav_idx"; //id number of active waypoint in legs above (int)
	private final String ACTIVE_WAYPOINT = "laminar/B738/fms/fpln_nav_id:string";

	//laminar/B738/fms/legs_mod_active // flight plan modification active
	//laminar/B738/fms/legs_alt_calc //only when plan is active

	//laminar/B738/vnav/legs_num ?? do i need it, can get from array.length

	//laminar/B738/fms/missed_app_wpt_idx 
	//laminar/B738/fms/vnav_decel_idx ??
	
	
	
	//laminar/B738/nd/wpt_x[20]
	//laminar/B738/nd/wpt_y[20]
	//laminar/B738/nd/wpt_id00m to 19 // magenta
	//laminar/B738/nd/wpt_id00w to 19 // white
	//laminar/B738/nd/wpt_type00 to 19 // 1 = white, 2 = magenta ??
	//laminar/B738/nd/wpt_eta00m to 19 //empty??
	//laminar/B738/nd/wpt_eta00w to 19 //empty??
	//laminar/B738/nd/wpt_alt00m to 19 //empty ??
	//laminar/B738/nd/wpt_alt00w to 19 //empty ??

	//laminar/B738/fms/lnav_dist2_next ?? probably waypoint after next

	//laminar/B738/pfd/ils_runway0 // name
	//laminar/B738/pfd/ils_show0 // show runway
	//laminar/B738/pfd/ils_rotate0
	//laminar/B738/pfd/ils_x0
	//laminar/B738/pfd/ils_y0




	public _FMS(ExtPlaneInterface iface) {
		this.iface = ExtPlaneInterface.getInstance();
		init();
		

		// fms_data
		Observer<DataRef> misc = new Observer<DataRef>() { // create FMS objects when data changes

			@Override
			public void update(DataRef object) {
				
				for(int i = 0; i < 2; i++) {
					if(object.getName().equals(ILS_ROTATE0[i])) {
						ils_rotate0[i] = Float.parseFloat(object.getValue()[0]);
					}
				}
			}
		};

		for(int i = 0; i < 2; i++) {
			iface.includeDataRef(ILS_ROTATE0[i]);
			iface.observeDataRef(ILS_ROTATE0[i], misc);
		}
		
		// fms_data
		Observer<DataRef> fms_data = new Observer<DataRef>() { // create FMS objects when data changes

			@Override
			public void update(DataRef object) {
				
				switch (object.getName()) {
				case ANP:
					anp = Float.parseFloat(object.getValue()[0]);
					break;
				case RNP:
					rnp = Float.parseFloat(object.getValue()[0]);
					break;
				case TRANS_ALT:
					trans_alt = Integer.parseInt(object.getValue()[0]);
					break;
				case FPLN_NAV_ID:
					fpln_nav_id = object.getValue()[0].replaceAll("\"", "");
					break;
				case FMS_VREF:
					fms_vref = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_15:
					fms_vref_15 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_25:
					fms_vref_25 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_30:
					fms_vref_30 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_40:
					fms_vref_40 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VREF_BUGS:
					fms_vref_bugs = Integer.parseInt(object.getValue()[0]);
					break;
				case FPLN_NAV_ID_ETA:
					fpln_nav_id_eta = object.getValue()[0].replaceAll("\"", "");
					break;
				case FPLN_NAV_ID_DIST:
					fpln_nav_id_dist = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_VR_SET:
					fms_vr_set = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_V1_SET:
					fms_v1_set = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_V2_15:
					fms_v2_15 = Float.parseFloat(object.getValue()[0]);
					break;
				case FMS_LEGS_NUM2:
					fms_legs_num2 = Integer.parseInt(object.getValue()[0]);
					break;
				case FMS_LEGS_NUM:
					fms_legs_num = Integer.parseInt(object.getValue()[0]);
					break;
				case LEGS_MOD_ACTIVE:
					legs_mod_active = Integer.parseInt(object.getValue()[0]);
					break;
				case MISSED_APP_WPT_IDX:
					missed_app_wpt_idx = Integer.parseInt(object.getValue()[0]);
					break;
				case MISSED_APP_ACTIVE:
					missed_app_active = Integer.parseInt(object.getValue()[0]);
					break;
				}
				for(int i = 0; i < 2; i++) {
					if(object.getName().equals(LEGS_STEP_CTR_IDX[i])) {
						legs_step_ctr_idx[i] = Integer.parseInt(object.getValue()[0]);
					}
				}
			}
		};
		iface.includeDataRef(ANP, 0.001f);
		iface.includeDataRef(RNP, 0.001f);
		iface.includeDataRef(TRANS_ALT);
		iface.includeDataRef(FPLN_NAV_ID);
		iface.includeDataRef(FMS_VREF);
		iface.includeDataRef(FMS_VREF_15);
		iface.includeDataRef(FMS_VREF_25);
		iface.includeDataRef(FMS_VREF_30);
		iface.includeDataRef(FMS_VREF_40);
		iface.includeDataRef(FMS_VREF_BUGS);
		iface.includeDataRef(FPLN_NAV_ID_ETA);
		iface.includeDataRef(FPLN_NAV_ID_DIST);
		iface.includeDataRef(FMS_VR_SET);
		iface.includeDataRef(FMS_V1_SET);
		iface.includeDataRef(FMS_V2_15);
		iface.includeDataRef(FMS_LEGS_NUM2);
		iface.includeDataRef(FMS_LEGS_NUM);
		iface.includeDataRef(LEGS_MOD_ACTIVE);
		iface.includeDataRef(MISSED_APP_WPT_IDX);
		iface.includeDataRef(MISSED_APP_ACTIVE);

		iface.observeDataRef(ANP, fms_data);
		iface.observeDataRef(RNP, fms_data);
		iface.observeDataRef(TRANS_ALT, fms_data);
		iface.observeDataRef(FPLN_NAV_ID, fms_data);
		iface.observeDataRef(FMS_VREF, fms_data);
		iface.observeDataRef(FMS_VREF_15, fms_data);
		iface.observeDataRef(FMS_VREF_25, fms_data);
		iface.observeDataRef(FMS_VREF_30, fms_data);
		iface.observeDataRef(FMS_VREF_40, fms_data);
		iface.observeDataRef(FMS_VREF_BUGS, fms_data);
		iface.observeDataRef(FPLN_NAV_ID_ETA, fms_data);
		iface.observeDataRef(FPLN_NAV_ID_DIST, fms_data);
		iface.observeDataRef(FMS_VR_SET, fms_data);
		iface.observeDataRef(FMS_V1_SET, fms_data);
		iface.observeDataRef(FMS_V2_15, fms_data);
		iface.observeDataRef(FMS_LEGS_NUM2, fms_data);
		iface.observeDataRef(FMS_LEGS_NUM, fms_data);
		iface.observeDataRef(LEGS_MOD_ACTIVE, fms_data);
		iface.observeDataRef(MISSED_APP_WPT_IDX, fms_data);
		iface.observeDataRef(MISSED_APP_ACTIVE, fms_data);
		
		for(int i = 0; i < 2; i++) {
			iface.includeDataRef(LEGS_STEP_CTR_IDX[i]);
			iface.observeDataRef(LEGS_STEP_CTR_IDX[i], fms_data);
		}

		// fms_legs
		Observer<DataRef> active_waypoints = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				if(object.getName().equals(LEGS)) {
					byte[] legsByte = Base64.getDecoder().decode(object.getValue()[0]);
					processActiveLegs(legsByte);
				}
				if(object.getName().equals(LEGS_LAT)) {
					for(int i = 0; i < 128; i++) {
						legs_lat[i] = Float.parseFloat(object.getValue()[i]);
					}
					updateActiveLat();
				}
				if(object.getName().equals(LEGS_LON)) {
					for(int i = 0; i < 128; i++) {
						legs_lon[i] = Float.parseFloat(object.getValue()[i]);
					}
					updateActiveLon();
				}
				if(object.getName().equals(LEGS_ALT_REST1)) {
					for(int i = 0; i < 128; i++) {
						legs_alt_rest1[i] = Float.parseFloat(object.getValue()[i]);
						_FMS.active_entries[i].setAlt_rest(Float.parseFloat(object.getValue()[i]));
					}
				}
				if(object.getName().equals(LEGS_ALT_REST2)) {
					for(int i = 0; i < 128; i++) {
						legs_alt_rest2[i] = Float.parseFloat(object.getValue()[i]);
						_FMS.active_entries[i].setAlt_rest_2(Float.parseFloat(object.getValue()[i]));
					}
				}
//				if(object.getName().equals(LEGS_ALT_REST1_2)) {
//					for(int i = 0; i < 128; i++) {
//						legs_alt_rest1_2[i] = Float.parseFloat(object.getValue()[i]);
//						FMS.active_entries[i].setAlt_rest_2(Float.parseFloat(object.getValue()[i]));
//					}
//				}
				if(object.getName().equals(LEGS_ALT_REST_TYPE1)) {
					for(int i = 0; i < 128; i++) {
						legs_alt_rest_type1[i] = Float.parseFloat(object.getValue()[i]);
						_FMS.active_entries[i].setAlt_rest_type(Float.parseFloat(object.getValue()[i]));
					}
				}
				if(object.getName().equals(LEGS_TYPE)) {
					for(int i = 0; i < 128; i++) {
						legs_type[i] = Float.parseFloat(object.getValue()[i]);
						_FMS.active_entries[i].setType(Float.parseFloat(object.getValue()[i]));
					}
				}
				if(object.getName().equals(FPLN_NAV_ID)) {
					active_waypoint = object.getValue()[0].replaceAll("\"", "");
				}
				for(int i = 0; i < 2; i++) {
					if(object.getName().equals(FPLN_ACTIVE[i])) {
						fpln_active[i] = Integer.parseInt(object.getValue()[0]);
					}
				}
			}

		};
		iface.includeDataRef(LEGS);
		iface.includeDataRef(LEGS_LAT);
		iface.includeDataRef(LEGS_LON);
		iface.includeDataRef(LEGS_ALT_REST1);
		iface.includeDataRef(LEGS_ALT_REST2);
		//iface.includeDataRef(LEGS_ALT_REST1_2);
		iface.includeDataRef(LEGS_ALT_REST_TYPE1);
		iface.includeDataRef(LEGS_TYPE);
//		iface.includeDataRef(ACTIVE_WAYPOINT);
		iface.observeDataRef(LEGS, active_waypoints);
		iface.observeDataRef(LEGS_LAT, active_waypoints);
		iface.observeDataRef(LEGS_LON, active_waypoints);
		iface.observeDataRef(LEGS_ALT_REST1, active_waypoints);
		iface.observeDataRef(LEGS_ALT_REST2, active_waypoints);
		//iface.observeDataRef(LEGS_ALT_REST1_2, active_waypoints);
		iface.observeDataRef(LEGS_ALT_REST_TYPE1, active_waypoints);
		iface.observeDataRef(LEGS_TYPE, active_waypoints);
		iface.observeDataRef(FPLN_NAV_ID, active_waypoints);
		for(int i = 0; i < 2; i++) {
			iface.includeDataRef(FPLN_ACTIVE[i]);
			iface.observeDataRef(FPLN_ACTIVE[i], active_waypoints);
		}
		
		Observer<DataRef> modified_waypoints = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				if(object.getName().equals(LEGS_2)) {
					byte[] legsByte = Base64.getDecoder().decode(object.getValue()[0]);
					processModifiedLegs(legsByte);
				}
				if(object.getName().equals(LEGS_LAT_2)) {
					for(int i = 0; i < 128; i++) {
						legs_lat_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					updateModifiedLat();
				}
				if(object.getName().equals(LEGS_LON_2)) {
					for(int i = 0; i < 128; i++) {
						legs_lon_2[i] = Float.parseFloat(object.getValue()[i]);
					}
					updateModifiedLon();
				}
			}

		};
		iface.includeDataRef(LEGS_2);
		iface.includeDataRef(LEGS_LAT_2);
		iface.includeDataRef(LEGS_LON_2);
//		iface.includeDataRef(ACTIVE_WAYPOINT);
		iface.observeDataRef(LEGS_2, modified_waypoints);
		iface.observeDataRef(LEGS_LAT_2, modified_waypoints);
		iface.observeDataRef(LEGS_LON_2, modified_waypoints);
//		iface.observeDataRef(FPLN_NAV_ID, active_waypoints);
//		for(int i = 0; i < 2; i++) {
//			iface.includeDataRef(FPLN_ACTIVE[i]);
//			iface.observeDataRef(FPLN_ACTIVE[i], active_waypoints);
//		}

		// fms_wpt
		Observer<DataRef> fms_wpt = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {


			}
		};

	}

	private void init() {
		_FMS.active_entries = new ActiveWayPoint[128];
		for (int i = 0; i < 128; i++) {
			_FMS.active_entries[i] = new ActiveWayPoint();
		}
		_FMS.modified_entries = new ModifiedWayPoint[128];
		for (int i = 0; i < 128; i++) {
			_FMS.modified_entries[i] = new ModifiedWayPoint();
		}
		_FMS.active_legs = new ActiveLegs[64];
		for (int i = 0; i < 64; i++) {
			_FMS.active_legs[i] = new ActiveLegs(i);
		}
		_FMS.mod_legs = new ModifiedLegs[64];
		for (int i = 0; i < 64; i++) {
			_FMS.mod_legs[i] = new ModifiedLegs(i);
		}
	}
	private void processActiveLegs(byte[] legsByte) {
		String decodedString = new String(legsByte);
		waypoints = decodedString.split("\\s+");
		updateActiveName();
		origin = waypoints[0];
		dest = waypoints[waypoints.length - 1];
	}
	private void processModifiedLegs(byte[] legsByte) {
		String decodedString = new String(legsByte);
		waypoints_2 = decodedString.split("\\s+");
		updateModifiedName();
		//origin = legs_2[0];
		//dest = legs[legs.length - 1];
	}
	private void updateActiveName() {
		if (waypoints != null) {
			for(int i = 0; i < waypoints.length; i++ ) {
				_FMS.active_entries[i].setId(i);
				_FMS.active_entries[i].setName(waypoints[i]);
				
				if(waypoints[i].equals(this.active_waypoint)) {
					_FMS.active_entries[i].setActive(true);
				}
			}
			for(int i = 0; i < waypoints.length - 1; i++ ) {
				_FMS.active_legs[i].setStartName(waypoints[i]);
				_FMS.active_legs[i].setEndName(waypoints[i + 1]);
			}
		}
	}
	private void updateModifiedName() {
		if (waypoints_2 != null) {
			for(int i = 0; i < waypoints_2.length; i++ ) {
				_FMS.modified_entries[i].setId(i);
				_FMS.modified_entries[i].setName(waypoints_2[i]);
			}
			for(int i = 0; i < waypoints_2.length - 1; i++ ) {
				_FMS.mod_legs[i].setStartName(waypoints_2[i]);
				_FMS.mod_legs[i].setEndName(waypoints_2[i + 1]);
			}
		}
	}
	private void updateActiveLat() {

		for(int i = 0; i < legs_lat.length; i++ ) {
			_FMS.active_entries[i].setLat(legs_lat[i]);
		}
		for(int i = 0; i < 64 - 1; i++) {
			_FMS.active_legs[i].setStartLat(legs_lat[i]);
			_FMS.active_legs[i].setEndLat(legs_lat[i + 1]);
		}
	}
	private void updateActiveLon() {
		for(int i = 0; i < legs_lon.length; i++ ) {
			_FMS.active_entries[i].setLon(legs_lon[i]);
		}
		for(int i = 0; i < 64 - 1; i++) {
			_FMS.active_legs[i].setStartLon(legs_lon[i]);
			_FMS.active_legs[i].setEndLon(legs_lon[i + 1]);
		}
	}
	private void updateModifiedLat() {
		for(int i = 0; i < legs_lat_2.length; i++ ) {
			_FMS.modified_entries[i].setLat(legs_lat_2[i]);
		}
		for(int i = 0; i < 64 - 1; i++) {
			_FMS.mod_legs[i].setStartLat(legs_lat_2[i]);
			_FMS.mod_legs[i].setEndLat(legs_lat_2[i + 1]);
		}
	}
	private void updateModifiedLon() {
		for(int i = 0; i < legs_lon_2.length; i++ ) {
			_FMS.modified_entries[i].setLon(legs_lon_2[i]);
		}
		for(int i = 0; i < 64 - 1; i++) {
			_FMS.mod_legs[i].setStartLon(legs_lon_2[i]);
			_FMS.mod_legs[i].setEndLon(legs_lon_2[i + 1]);
		}
	}
}
